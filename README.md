# README #

Currently, this repository is made available only for the purpose of the anonymized peer review of a research paper. On the publication of the research paper, this repository will be published as a public repository under the CC BY-NC-SA license.

### What is this repository for? ###

* This repository contains the fabrication files (Assemly files, Drill files and Gerber files) of the TronicBoards Electronics Learning Kit for its 14 boards: 
	* Power Boards:
		* Battery Board
		* USB Board
	* Action Boards:
		* Light Board
		* Light Mixer Board
		* Alarm Board
		* Music Board
		* Fan Board
		* Vibration Board
	* Sensor Boards:
		* Push Button Board
		* Tilt Switch
		* Reed Switch
		* Light Sensor Board
		* Temperature Sensor Board
		* Touch Sensor Boards

* Currently, this repository is made available only for the purpose of the anonymized peer review of a research paper. 

* Upon the publication of that research paper, this repository will be published as a public repository under the CC BY-NC-SA license.

### Acknowledgments ###

* Anonymized for review

### License ###

* CC BY-NC-SA